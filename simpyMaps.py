
import random
import simpy
import gmaps
from drawMaps import drawMap
import json
from gmaps import getPotentialRestaurant

def source(env, number, interval, rider):
    """Source generates customers randomly"""
    for i in range(number):                   
        dt,poly,userLocation=gmaps.main(resLat,resLng,travelMode) #userLocation is an object
        polyCollection.append(poly)
        userLocationsList.append(userLocation)
        dt=float(dt/60)     #dt:restaurant to user              
        c = order(env, 'Order%02d' % i, rider,dt)
        env.process(c)        
        t = random.expovariate(1.0 / interval) 
        yield env.timeout(t)

def order(env, name, rider,dt):
    """order createds, is served and leaves."""     
    created = env.now
    pickupTime=''
    deliveredTime=''
    backTime=''
    canceled=False
    canceldTime=''   

    print('%7.4f %s: Order Created' % (created, name))

    with rider.request() as req: 
        #The request() method generates an event that lets you wait 
        #until the resource becomes available again
        
        # Wait for the rider or abort at the end of our tether
        results = yield req | env.timeout(ACCEPTABLE_TIME)
       
        wait = env.now - created

        if req in results:
            # We got to the rider
            print('%7.4f %s: picked up, waited for %6.3f' % (env.now, name, wait))
            pickupTime=env.now
            yield env.timeout(dt)
                  
            deliveredTime=env.now
            print('%7.4f %s: delivered' % (env.now, name))

            yield env.timeout(dt)          

            print('%7.4f %s: ride for this order back to restaurant' % (env.now,name))
            backTime=env.now
        else:
            print('%7.4f %s canceld' % (env.now, name))
            canceled=True
            canceldTime=env.now

    if canceled:    
        orderDict = {
                ' create at ': created,                
                ' canceld at ': canceldTime
            }
    else:
        orderDict = {                
                ' create at ': created,
                ' pick up at': pickupTime,    
                ' delivered at': deliveredTime,        
                ' rider back at ': backTime 
            }
    
    simulationInfoDict[name]=orderDict 
    
ORDERS = 10  # Total number of orders 
INTERVAL_ORDERS = 5 # Generate new orders roughly every 10 min
ACCEPTABLE_TIME=3 #acceptable waiting time for pick up

while True:
    userLocation=input('please input your address: ')
    if len(getPotentialRestaurant(userLocation))>1:
        print('please select from: '+str(getPotentialRestaurant(userLocation)))
        continue
    elif len(getPotentialRestaurant(userLocation))==0:
        print('please input correct location')
    else:
        break

resLat,resLng,travelMode,riderTotal=gmaps.getRestaurant(userLocation)
polyCollection=[]
userLocationsList=[]
simulationInfoDict={}
         
# Setup and start the simulation
print('order status')
env = simpy.Environment()

# Start processes and run
rider = simpy.Resource(env, capacity=riderTotal) #how many riders are able to be used

env.process(source(env, ORDERS, INTERVAL_ORDERS, rider))
env.run()
#print(polyCollection)

simulationInfoList=sorted(simulationInfoDict.items(),key=lambda x:x[0])
simulationResult={'simulation':simulationInfoList}
# print(simulationInfoList)

with open('simulation_summary.json', 'w') as file:
    json.dump(simulationInfoList, file, indent=4)

drawMap(resLat,resLng,userLocationsList,polyCollection)
gmaps.saveToMongodb('simulation_summary',simulationResult)
