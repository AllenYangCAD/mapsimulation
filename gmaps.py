import urllib.request
import json
import pymongo
from polyline.codec import PolylineCodec
import random
from api import apiKeyAugmenify
from math import sin, cos, acos


def getPotentialRestaurant(address):
    inputLocation = address.replace(' ', '+')
    
    locEndPoint = 'https://maps.googleapis.com/maps/api/place/textsearch/json?'
    apikey = apiKeyAugmenify
    loc_request = 'input={}&key={}'.format(inputLocation, apikey)

    request = locEndPoint + loc_request
    locResponse = urllib.request.urlopen(request).read()    

    restaurants = json.loads(locResponse)

    restaurantsAddress=[]
    for item in restaurants["results"]:
        restaurantsAddress.append(item["formatted_address"]) 
    
    # json_str = json.dumps(restaurants, indent=4)
    # with open('restuarant.json', 'w') as json_file:
    #     json_file.write(json_str)

    json_str2 = json.dumps(restaurantsAddress, indent=4)
    with open('restaurantsAddress.json', 'w') as jfile:
        jfile.write(json_str2)
        
    return restaurantsAddress

def getRestaurant(address):
    inputLocation = address.replace(' ', '+')
    inputType = 'textquery'
    fields = 'photos,formatted_address,name,geometry'
    locEndPoint = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?'
    apikey = apiKeyAugmenify
    loc_request = 'input={}&inputtype={}&fields={}&key={}'.format(inputLocation, inputType, fields, apikey)
    request = locEndPoint + loc_request
    locResponse = urllib.request.urlopen(request).read()    
    restaurantInfo = json.loads(locResponse)
    json_str = json.dumps(restaurantInfo, indent=4)
    with open('restaurantInfo.json', 'w') as json_file:
        json_file.write(json_str)
       
    candidateAccess= restaurantInfo['candidates']
    geometryAccess=candidateAccess[0]['geometry']
    resLocation=geometryAccess['location']
    resLat=resLocation['lat']
    resLng=resLocation['lng']
    travelOptions=["driving","bicycling","walking"]       
    while (True):
        travelMode=input("please choose travel mode from: driving, bicycling, walking: ") 
        if travelMode not in travelOptions:            
            continue
        else:
            break
    riderTotal=int(input("Please enter your rider numbers: "))    
    return resLat,resLng,travelMode,riderTotal

def getDirection(origin,destination,travelMode,dirEndpoint,apiKey):
        nav_request = 'origin={}&destination={}&mode={}&key={}'.format(origin, destination, travelMode,apiKey)
        dirRequest = dirEndpoint + nav_request

        dirResponse = urllib.request.urlopen(dirRequest).read()  ## dirRespnse is a JSON object

        directions = json.loads(dirResponse)  ## directions is dict
        ## json.dump and json.load are both methods to deal with JSON ENCODED FORMAT files(looks like json)

        json_str = json.dumps(directions, indent=4)
        with open('original_data.json', 'w') as json_file:
            json_file.write(json_str)
        return directions

def ranNumInRange():
    return (0.05*(random.random())-0.025)  
    
def getClosetRider(riderLocationArray, resLat, resLng):
    dist=[]
    for i in range(len(riderLocationArray)):        
        dist.append(6371.01 * acos(sin(riderLocationArray[i]["lat"])*sin(resLat) + cos(riderLocationArray[i]["lat"])*cos(resLat)*cos(riderLocationArray[i]["lng"] - resLng)))

    minDistanceRiderNumber=dist.index(min(dist))
    minDistanceRiderLat=riderLocationArray[minDistanceRiderNumber]['lat']
    minDistanceRiderLng=riderLocationArray[minDistanceRiderNumber]['lng']
    return str(minDistanceRiderLat)+','+str(minDistanceRiderLng)

def getWaypoints():
    
    WayPoints = ''
    while True:
        Choice = input("do you need to add other user address in the trip ?(Y/N)")
        j = 0

        if (Choice == "N" or Choice == "n"):
            return WayPoints

        if (Choice == "Y" or Choice == "y"):

            WayPoint = input("Please enter your " + str(j + 1) + " address: ")
            WayPoints += WayPoint + '|'
            j += 1
            while True:
                YN = input("the added address is: " + WayPoint + ", do you want to add more?(Y/N)")
                if (YN == "Y" or YN == "y"):
                    WayPoint = input("Please enter your " + str(j + 1) + " address: ")
                    WayPoints += WayPoint + '|'
                    j += 1

                if (YN == "N" or YN == "n"):
                    WayPoints = WayPoints[:-1]
                    return WayPoints
                else:
                    continue
        else:
            continue

def getDuration(directions,travelData):

    routeAccess = directions['routes']
    for routeData in routeAccess:
        legAccess = routeData['legs']
        for legData in legAccess:  # each legData is a dict

            del legData["steps"]
            del legData["traffic_speed_entry"]
            del legData['via_waypoint']
            travelData.append(legData)

        polyPointsAccess=routeData['overview_polyline']
        for polyPoint in polyPointsAccess:
            polyPoints=polyPointsAccess['points']
        return polyPoints

def saveToMongodb(collectionName,infoDict):
    client = pymongo.MongoClient('mongodb://localhost:27017')
    db = client.GoogleMaps    
    collection= db['%s'%(collectionName)]    
    result = collection.insert_one(infoDict)
    if result.acknowledged:
        print('results saved in databse ' + str(result.inserted_id))

def main(resLat,resLng,travelMode):
   
   
    ### introduce endpoints in
    dirEndpoint = 'https://maps.googleapis.com/maps/api/directions/json?'  

    #create user location
    userLat=resLat+ranNumInRange()
    userLng=resLng+ranNumInRange()

    userLocation={"lat": userLat,"lng":userLng}

    origin =str(resLat)+','+str(resLng)
    destination = str(userLat)+','+str(userLng)

    apiKey=apiKeyAugmenify
    directions=getDirection(origin,destination,travelMode,dirEndpoint,apiKey)

    travelData = []       
    polyPoints=getDuration(directions,travelData)

    travelDataDict = {
        'origin': travelData[0]["start_address"],
        'destination': travelData[-1]["end_address"],
        'trip_info': travelData,
        'delivery_mode':travelMode
    }
    
    #firstPeriod=travelDataDict['each_segment_info'][0]['duration']['value']
    secondePeriod=travelDataDict['trip_info'][0]['duration']['value']

    with open('travelData.json', 'w') as file:
        json.dump(travelDataDict, file, indent=4)

    ### save result into mongodb
    saveToMongodb('Directions',travelDataDict)
    poly=PolylineCodec().decode(polyPoints) #all polylines for the trip    
    return secondePeriod,poly,userLocation


   
    



