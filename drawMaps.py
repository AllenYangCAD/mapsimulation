
import gmplot
#from myPackage.Car import Car

from api import apiKeyAugmenify
import webbrowser
import random
def randomColor():
    colorArr = ['1','2','3','4','5','6','7','8','9','A','B','C','D','E','F']
    color = ""
    for i in range(6):
        color += colorArr[random.randint(0,14)]
    return "#"+color


def drawMap(resLat,resLng,userLocationList,polyCollection):
    gmapOne = gmplot.GoogleMapPlotter(resLat,resLng, 14, apikey=apiKeyAugmenify) #take restaurant as center of the map
    #userLocationList[o] is restaurant location, others are the users address

    # polyLatList=[]
    # polylngList=[]
    # for pair in polyCollection:
    #     polyLatList.append(pair[0])
    #     polylngList.append(pair[1])

    for i in range(len(polyCollection)):
        polyLatList=[]
        polylngList=[]
        for pair in polyCollection[i]:
            polyLatList.append(pair[0])
            polylngList.append(pair[1])
        gmapOne.scatter(polyLatList,polylngList,'red',size=1,marker=False) #plots the points on the map
        gmapOne.plot(polyLatList,polylngList,randomColor(),edge_width=3) #joins all the points

    #used to mark rider locations
    # for i in range(len(riderLocations)):
    #     gmapOne.marker(riderLocations[i]["lat"],riderLocations[i]["lng"],title='rider'+str(i+1))
    
    gmapOne.marker(resLat,resLng,title='Restaurant') #mark the origin
    for i in range(len(userLocationList)):
        gmapOne.marker(userLocationList[i]['lat'],userLocationList[i]['lng'],title=('User Address'+str(i+1))) # mark the destination

    gmapOne.draw("map.html")
    webbrowser.open("map.html")

